<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Pagination\CursorPaginator;
use App\Models\Product;
use File;
use Storage;

class ProductController extends Controller
{
    public function index(){
        // $i = Product::all();
        $x = Product::paginate(2);
        return view('product.list', compact(['x']));
    }
    public function create(){
        return view('product.create');
    }
    public function createSubmit(Request $r){
        $image_path = $r->name.'-'.time().'.'.$r->raw_image->extension();  
        $r->raw_image->move(public_path('uploads'), $image_path);
        $r->merge([ 'image_path' => $image_path ]);
     
        Product::create($r->except(['_token']));
        return redirect('/products');
    }
    public function edit($id){
        $i = Product::find($id);
        return view('product.edit', compact(['i']));
    }
    public function editSubmit($id, Request $r){
        if(!empty($r->has('raw_image'))){
            $image_path = $r->name.'-'.time().'.'.$r->raw_image->extension();  
            $r->raw_image->move(public_path('uploads'), $image_path);
            $r->merge([ 'image_path' => $image_path ]);
        };

        $i = Product::find($id);
        $i->update($r->except(['_token']));
        return redirect('/products');
    }
    public function delete($id){
        $i = Product::find($id);
        return view('product.delete', compact(['i']));
    }
    public function deleteSubmit($id){
        $i = Product::find($id);
        File::delete(public_path('uploads/'.$i->image_path));
        $i->delete();
        return redirect('/products');
    }
}
