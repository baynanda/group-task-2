<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use Illuminate\Support\Facades\Auth;

class AuthController extends Controller
{
    public function index(){
        return view('login');
    }
    public function login(Request $r){
        $r->validate([
            'username' => 'required',
            'password' => 'required',
        ]);
   
        if (Auth::attempt(['username' => $r->username, 'password' => $r->password])) {
            $r->session()->regenerate();
            return redirect()->intended('/products');
        }
        return redirect("login")->withSuccess('Login details are not valid');
    }

    public function logout(Request $r){
        Auth::logout();
        $r->session()->invalidate();
        $r->session()->regenerateToken();
        return redirect('/');
    }
}
