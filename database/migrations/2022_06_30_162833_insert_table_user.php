<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('table_user', function (Blueprint $table) {
            //
        });

        DB::table('table_user')->insert(
            array(
                'username' => 'admin',
                'role' => 'admin',
                'description' => 'admin user',
                'birthPlace' => 'laravel',
                'birthDate' => '2022-06-01',
                'gender' => 'M',
                'email' => 'admin@email.com',
                'password' => Hash::make('123'),
            )
        );
        DB::table('table_user')->insert(
            array(
                'username' => 'viewer',
                'role' => 'viewer',
                'description' => 'viewer user',
                'birthPlace' => 'laravel',
                'birthDate' => '2022-06-01',
                'gender' => 'F',
                'email' => 'viewer@email.com',
                'password' => Hash::make('123')
            )
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('table_user', function (Blueprint $table) {
            //
        });
    }
};
