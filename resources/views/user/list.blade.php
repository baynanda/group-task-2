@extends('layout.main')

@section('content')
<div class="row container-fluid justify-content-center">
    <div class="col-10 pb-3">
        <a href="/user/create" class="btn btn-success @if(Auth::user()->role == 'viewer') disabled @endif">Create <i
                class="fa fa-plus-circle" aria-hidden="true"></i></a>
    </div>
    <div class="col-10">
        <table class="table table-striped table-hover table-bordered">
            <tr>
                <th>No</th>
                <th>Username</th>
                <th>Description</th>
                <th>Birth Place</th>
                <th>Birth Date</th>
                <th>Email</th>
                <th>Role</th>
                <th>Action</th>
            </tr>
            <?php $a=1;?>
            @foreach($i as $i)
            <tr>
                <td>{{$a}}</td>
                <td>{{$i->username}}</td>
                <td>{{$i->description}}</td>
                <td>{{$i->birthPlace}}</td>
                <td>{{$i->birthDate}}</td>
                <td>{{$i->email}}</td>
                <td>{{$i->role}}</td>
                <td>
                    <a href="/user/{{$i->id}}/edit"
                        class="btn btn-warning text-white @if(Auth::user()->role == 'viewer') disabled @endif"><i
                            class="fa fa-pencil-square-o" aria-hidden="true"></i></a>
                    <a href="/user/{{$i->id}}/delete"
                        class="btn btn-danger text-white @if(Auth::user()->role == 'viewer' || Auth::user()->username == $i->username) disabled @endif"><i
                            class="fa fa-trash" aria-hidden="true"></i></a>
                </td>
            </tr>
            <?php $a++;?>
            @endforeach
        </table>
    </div>
</div>
@endsection
